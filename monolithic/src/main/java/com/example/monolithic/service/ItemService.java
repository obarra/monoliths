package com.example.monolithic.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.monolithic.model.Item;
import com.example.monolithic.model.entity.Product;



@Service()
public class ItemService{
	
	private final static Long MAGIC_NUMBER = 100L;
	
	@Autowired
	private ProductService productService;
	
	public List<Item> findAll() {
		List<Product> products = productService.findAll();
		List<Item> items = products.stream().map(p -> new Item(p, MAGIC_NUMBER))
				.collect(Collectors.toList());
		return items;
	}

	public Item findById(Long id) {
		Product product = productService.findById(id);
		return new Item(product, MAGIC_NUMBER);
	}

	public Product save(Product product) {
		return productService.save(product);
	}

	public Product update(Product product, Long id) {
		Product productDataBase = productService.findById(id);
		productDataBase.setName(product.getName());
		productDataBase.setPrice(product.getPrice());
		return productService.save(productDataBase);
	}

	public void delete(Long id) {
		productService.deleteById(id);
	}

}
