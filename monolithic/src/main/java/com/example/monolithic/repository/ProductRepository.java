package com.example.monolithic.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.monolithic.model.entity.Product;


public interface ProductRepository extends CrudRepository<Product, Long>{

}
